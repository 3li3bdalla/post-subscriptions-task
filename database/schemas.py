from pydantic import BaseModel, SecretStr, validator
from password_strength import PasswordPolicy
from repositories.user import get_user_by_username

password_policy = PasswordPolicy.from_names(
    length=8,  # min length: 8
    uppercase=1,  # need min. 2 uppercase letters
    numbers=1,  # need min. 2 digits
    special=1,  # need min. 2 special characters
)


class UserBase(BaseModel):
    username: str
    password: str

    @validator('password')
    def password_complexity(cls, v):
        for test in list(password_policy.test(v)):
            raise ValueError("password should contain {}".format(test.name()))
        return v

    @validator("username")
    def required_username(cls, v):
        if len(v) < 2:
            raise ValueError("username should be at least 3 latter!")
        return v

    @validator("password")
    def required_password(cls, v):
        if len(v) == 0:
            raise ValueError("password should be provided!")
        return v


class UserCreate(UserBase):
    @validator("username")
    def is_unique_username(cls, v):
        is_exists = get_user_by_username(v)
        if is_exists is not None:
            raise ValueError("username has already used!")
        return v


class UserLogin(UserBase):
    pass


class User(UserBase):
    id: int

    class Config:
        orm_mode = True


class PostBase(BaseModel):
    title: str
    body: str


class PostCreate(PostBase):
    pass
