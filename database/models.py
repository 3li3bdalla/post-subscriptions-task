from config.database import Base
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, text
from sqlalchemy.orm import relationship


class Post(Base):
    __tablename__ = "posts"
    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("users.id"))
    title = Column(String, index=True)
    body = Column(String, index=False)
    created_at = Column(DateTime, server_default=text('NOW()'))
    user = relationship("User", back_populates="posts", foreign_keys=[user_id])


class Subscriber(Base):
    __tablename__ = "subscribers"
    id = Column(Integer, primary_key=True, index=True)
    subscriber_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    subscribed_to_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    created_at = Column(DateTime, server_default=text('NOW()'))
    subscribed_to = relationship("User", back_populates="subscribers", foreign_keys=[subscribed_to_id])
    subscriber = relationship("User", back_populates="subscribed_to", foreign_keys=[subscriber_id])


class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, index=True)
    password = Column(String)
    created_at = Column(DateTime, server_default=text('NOW()'))
    posts = relationship("Post", back_populates="user", foreign_keys=[Post.user_id])
    subscribers = relationship("Subscriber", back_populates="subscriber", foreign_keys=[Subscriber.subscriber_id])
    subscribed_to = relationship("Subscriber", back_populates="subscribed_to",
                                 foreign_keys=[Subscriber.subscribed_to_id])
