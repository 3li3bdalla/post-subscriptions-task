"""create subscribers table

Revision ID: e6b0f4193bb2
Revises: dc453703576e
Create Date: 2022-09-29 10:18:21.512409

"""
from alembic import op
import sqlalchemy as sa
import datetime

# revision identifiers, used by Alembic.
revision = 'e6b0f4193bb2'
down_revision = 'dc453703576e'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'subscribers',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('subscriber_id', sa.Integer, sa.ForeignKey('users.id')),
        sa.Column('subscribed_to_id', sa.Integer, sa.ForeignKey('users.id')),
        sa.Column('created_at', sa.TIMESTAMP, server_default=sa.func.now())
    )


def downgrade() -> None:
    op.drop_table('subscribers')
