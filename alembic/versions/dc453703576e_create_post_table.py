"""create post table

Revision ID: dc453703576e
Revises: 80803684dd7e
Create Date: 2022-09-29 10:18:11.883448

"""
from alembic import op
import sqlalchemy as sa
import datetime

# revision identifiers, used by Alembic.
revision = 'dc453703576e'
down_revision = '80803684dd7e'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'posts',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_id', sa.Integer, sa.ForeignKey('users.id')),
        sa.Column('title', sa.String(100)),
        sa.Column('body', sa.Unicode(1000)),
        sa.Column('created_at', sa.TIMESTAMP, server_default=sa.func.now())
    )


def downgrade() -> None:
    op.drop_table('posts')
