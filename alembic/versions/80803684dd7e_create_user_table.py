"""create user table

Revision ID: 80803684dd7e
Revises: 
Create Date: 2022-09-29 10:18:08.582802

"""
from alembic import op
import sqlalchemy as sa
import datetime

# revision identifiers, used by Alembic.
revision = '80803684dd7e'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'users',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('username', sa.String(50), nullable=False, unique=True),
        sa.Column('password', sa.String(200)),
        sa.Column('created_at', sa.TIMESTAMP, server_default=sa.func.now())
    )


def downgrade() -> None:
    op.drop_table('users')
