from kernel import app as kernelApp
from fastapi import FastAPI, Request
from controllers.users import router as users_router
from controllers.subscribers import router as subscribers_router
from controllers.posts import router as posts_router

app = FastAPI()

kernelApp.bootstrap(app)

app.include_router(users_router)
app.include_router(posts_router)
app.include_router(subscribers_router)


@app.get("/")
async def root():
    return {"message": "Embed Python/DevOps assignment"}
