import datetime
import os

from database import models
from config.database import get_db
from fastapi.exceptions import HTTPException
from config.database import paginate

MAX_NUMBER_OF_SUBSCRIPTIONS = os.getenv("MAX_NUMBER_OF_SUBSCRIPTIONS", 100)
db = get_db()


def is_subscribed_to(subscriber_id: int, subscribed_to_id: int):
    return db.query(models.Subscriber).filter(
        models.Subscriber.subscriber_id == subscriber_id).filter(
        models.Subscriber.subscribed_to_id == subscribed_to_id).first()


def _perform_subscribe(subscriber_id, subscribe_to_id):
    db_subscribe = models.Subscriber(subscriber_id=subscriber_id, subscribed_to_id=subscribe_to_id)
    db.add(db_subscribe)
    db.commit()
    db.refresh(db_subscribe)
    return db_subscribe


def _perform_unsubscribe(exists_subscription: models.Subscriber):
    db.delete(exists_subscription)
    db.commit()


def reached_maximum_number_of_subscriptions(subscriber_id):
    return db.query(models.Subscriber).filter(
        models.Subscriber.subscriber_id == subscriber_id).count() >= MAX_NUMBER_OF_SUBSCRIPTIONS


def trying_to_subscribe_same_user(subscriber_id: int, subscriber_to_id: int) -> bool:
    return subscriber_id == subscriber_to_id


def subscribe_to(subscriber_id: int, subscribe_to_id: int):
    if trying_to_subscribe_same_user(subscriber_id, subscribe_to_id):
        raise HTTPException(
            status_code=400,
            detail="Sorry You can't subscribe to your self")
    if reached_maximum_number_of_subscriptions(subscriber_id):
        raise HTTPException(
            status_code=400,
            detail="You have reached the maximum number of subscriptions, You have {} subscriptions now, you should "
                   "unsubscribe first".format(
                MAX_NUMBER_OF_SUBSCRIPTIONS),
        )

    exists_subscription = is_subscribed_to(subscriber_id, subscribe_to_id)
    if not exists_subscription:
        return _perform_subscribe(subscriber_id, subscribe_to_id)
    raise HTTPException(
        status_code=400,
        detail="You are already subscribed to this user",
    )


def unsubscribe_to(subscriber_id: int, un_subscribe_to_id: int):
    exists_subscription = is_subscribed_to(subscriber_id, un_subscribe_to_id)
    if exists_subscription:
        _perform_unsubscribe(exists_subscription)
        return True
    raise HTTPException(
        status_code=400,
        detail="You are not subscribe to this user",
    )


def get_follows_ids(subscriber_id: int):
    follows = db.query(models.Subscriber).filter(models.Subscriber.subscriber_id == subscriber_id).join(
        models.User.subscribed_to, isouter=True).all()
    ids = list()
    for user in follows:
        ids.append(user.subscribed_to_id)
    return ids


def get_subscribed_to_list(subscriber_id: int, page: int = 1):
    data = list()
    result = paginate(db.query(models.Subscriber).filter(models.Subscriber.subscriber_id == subscriber_id).join(
        models.User.subscribed_to, isouter=True), page)
    for user in result:
        data.append(user.subscribed_to.username)
    return data


def get_subscribers_list(subscribed_to_id: int, page: int = 1):
    data = list()
    result = paginate(db.query(models.Subscriber).filter(models.Subscriber.subscribed_to_id == subscribed_to_id).join(
        models.User.subscribers, isouter=True), page)
    for user in result:
        data.append(user.subscriber.username)
    return data
