import datetime
from database import models
from config.database import get_db
from passlib.hash import bcrypt
from fastapi.exceptions import HTTPException
from config.database import paginate

__db = get_db()


def get_user_by_id(user_id: int):
    return __db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_username(username: str):
    return __db.query(models.User).filter(models.User.username == username).first()


def get_users(page: int = 1):
    return paginate(__db.query(models.User).order_by(models.User.id), page)


def create_user(user):
    password_hash = bcrypt.encrypt(user.password)
    db_user = models.User(username=user.username, password=password_hash, created_at=datetime.datetime.now())
    __db.add(db_user)
    __db.commit()
    __db.refresh(db_user)
    return db_user


def is_valid_password(logged_user, password) -> bool:
    return bcrypt.verify(password, logged_user.password)


def get_users_usernames_with_subscribers(page: int = 1):
    data = []
    result = paginate(__db.query(models.User).join(models.User.subscribed_to, isouter=True).order_by(models.User.id),
                      page)
    for user in result:
        userData = {
            "username": user.username,
            "subscribers_count": len(user.subscribed_to),
        }
        data.append(userData)
    return data


def get_user_by_username_or_fail(username: str):
    user = get_user_by_username(username)
    if not user:
        raise HTTPException(
            status_code=404,
            detail="Username Not Found",
        )
    return user
