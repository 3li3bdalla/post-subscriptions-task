from database.models import User, Post, Subscriber
from config.database import get_db
from repositories.subscribe import get_follows_ids
from config.database import paginate

db = get_db()


def __apply_search_in_query(query, search=None, usernames=None, body=None):
    if search:
        query = query.filter(Post.title.ilike("%{}%".format(search)))
    if usernames and len(usernames) > 0:
        usernames = list(usernames.split(","));
        query = query.filter(User.username.in_(usernames))
    if body:
        query = query.filter(Post.body.ilike("%{}%".format(body)))
    return query


def get_user_timeline(user: User, search=None, usernames=None, body=None, page: int = 1):
    follows_ids = get_follows_ids(user.id)
    follows_ids.append(user.id)
    query = db.query(Post).filter(Post.user_id.in_(follows_ids)).join(User.posts, isouter=True).order_by(
        Post.created_at)
    posts = paginate(__apply_search_in_query(query, search, usernames, body), page)
    data = list()
    for post in posts:
        data.append(_format_post_cel(post))
    return data


def _format_post_cel(post: Post):
    return {
        "id": post.id,
        "username": post.user.username,
        "title": post.title,
        "body": post.body,
        "created_at": post.created_at,
        "user_id": post.user_id
    }


def get_user_posts(user: User, search=None, usernames=None, body=None, page: int = 1):
    query = db.query(Post).filter(Post.user_id == user.id).order_by(Post.created_at)
    return paginate(__apply_search_in_query(query, search, usernames, body), page)


def create_post(post, current_user):
    db_post = Post(title=post.title, body=post.body, user_id=current_user.id)
    db.add(db_post)
    db.commit()
    db.refresh(db_post)
    return db_post
