import random

from faker import Faker
from repositories.user import get_user_by_username, create_user
from repositories.post import create_post
from database.schemas import UserCreate, PostCreate
from repositories import subscribe


def add_demo_user(user_data):
    return create_user(UserCreate(username=user_data.get("username"), password=user_data.get("password")))


def add_demo_post(post_data, user):
    return create_post(
        PostCreate(title=post_data.get("title"), body=post_data.get("body")), user)


first_user = {
    "username": "demo",
    "password": "deMo@123@321b"
}

exists = get_user_by_username(first_user.get("username"))
if exists is None:
    first_created_user = add_demo_user(first_user)
    users = []
    for x in range(20):
        faker = Faker()
        created_faker_user = add_demo_user({
            "username": faker.name(),
            "password": faker.password()
        })
        for _ in range(random.randint(1, 5)):
            add_demo_post({
                "title": faker.text(80),
                "body": faker.text(500)}, created_faker_user)
        follow_first_user = bool(random.getrandbits(1))
        if follow_first_user:
            subscribe.subscribe_to(created_faker_user.id, first_created_user.id)
        followed_by_first_user = bool(random.getrandbits(1))
        if followed_by_first_user:
            subscribe.subscribe_to(first_created_user.id, created_faker_user.id)
