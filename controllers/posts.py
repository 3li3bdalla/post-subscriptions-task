from fastapi import APIRouter, Depends
from database.models import User
from config.auth import get_current_user
from repositories import post as post_repository
from database.schemas import PostCreate
from fastapi_redis_cache import cache
from fastapi import Query

router = APIRouter(
    prefix="/posts",
    tags=["posts"],
    dependencies=[Depends(get_current_user)],
    responses={404: {"description": "Not found"}},
)


@router.get("/", tags=["posts"])
@cache(expire=1)
async def get_posts(current_user: User = Depends(get_current_user), search_txt=None, body=None, usernames=None,
                    page: int = 1):
    return post_repository.get_user_posts(current_user, search_txt, usernames, body, page)


@router.get("/timeline", tags=["posts"])
# @cache(expire=1)
async def get_user_timeline(current_user: User = Depends(get_current_user), search_txt=None, body=None, usernames=None,
                            page: int = 1):
    return post_repository.get_user_timeline(current_user, search_txt, usernames, body, page)


@router.post("/", tags=["posts"])
async def create_post(post: PostCreate, current_user: User = Depends(get_current_user)):
    return post_repository.create_post(post, current_user)
