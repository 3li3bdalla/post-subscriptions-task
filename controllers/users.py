from fastapi import APIRouter
from repositories import user as userRepository
from database.schemas import UserCreate, UserLogin
from fastapi.exceptions import HTTPException
from config import auth
from fastapi_redis_cache import cache

router = APIRouter(
    prefix="/users",
    tags=["users"],
    responses={404: {"description": "Not found"}},
)


@router.get("/", tags=["users"])
@cache(expire=1)
async def get_users(page: int = 1):
    return userRepository.get_users(page)


@router.post("/register", tags=["users", "create-user"])
async def create_user(user: UserCreate):
    created_user = userRepository.create_user(user)
    return auth.generate_jwt_token(created_user)


@router.post("/login", tags=["users", "create-user"])
async def login_user(user: UserLogin):
    logged_user = userRepository.get_user_by_username(user.username)
    if logged_user is None or not userRepository.is_valid_password(logged_user, user.password):
        raise HTTPException(status_code=400, detail=[
            {
                "loc": [
                    "body",
                    "username"
                ],
                "msg": "username or password invalid",
                "type": "value_error"
            }
        ])
    return auth.generate_jwt_token(logged_user)


@router.get("/usernames", tags=["users"])
# @cache(expire=1)
async def get_users_usernames(page: int = 1):
    return userRepository.get_users_usernames_with_subscribers(page)
