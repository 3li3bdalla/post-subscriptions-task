from fastapi import APIRouter, Depends
from database.models import User
from config.auth import get_current_user
from repositories.user import get_user_by_username_or_fail
from repositories.subscribe import subscribe_to, get_subscribed_to_list, get_subscribers_list, unsubscribe_to
from fastapi_redis_cache import cache

router = APIRouter(
    prefix="/subscribers",
    tags=["subscribers"],
    dependencies=[Depends(get_current_user)],
    responses={404: {"description": "Not found"}},
)


@router.post("/{username}/subscribe", tags=["subscribers"])
async def subscribe(username: str, current_user: User = Depends(get_current_user)):
    wanna_subscribe_to = get_user_by_username_or_fail(username)
    subscribe_to(current_user.id, wanna_subscribe_to.id)
    return {"detail": "subscribed Successfully to {}".format(username)}


@router.post("/{username}/unsubscribe", tags=["subscribers"])
async def unsubscribe(username: str, current_user: User = Depends(get_current_user)):
    wanna_unsubscribe_to = get_user_by_username_or_fail(username)
    unsubscribe_to(current_user.id, wanna_unsubscribe_to.id)
    return {"detail": "unsubscribed Successfully to {}".format(username)}


@router.get("/subscribed_to", tags=["subscribers"])
@cache(expire=1)
def subscriptions(current_user: User = Depends(get_current_user), page: int = 1):
    return get_subscribed_to_list(current_user.id, page)


@router.get("/to_me", tags=["subscribers"])
@cache(expire=1)
def subscribers(current_user: User = Depends(get_current_user), page: int = 1):
    return get_subscribers_list(current_user.id, page)
