#!/bin/sh
alembic upgrade head
python generate-demo-data.py
uvicorn main:app --host 0.0.0.0 --port 8080 --reload