from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os

DB_HOST = os.getenv('DB_HOST')
DB_USERNAME = os.getenv('DB_USERNAME')
DB_NAME = os.getenv('DB_NAME')
DB_ENGINE = os.getenv('DB_ENGINE', "postgresql")
DB_PASSWORD = os.getenv('DB_PASSWORD')
SQLALCHEMY_DATABASE_URL = "{}://{}:{}@{}/{}".format(DB_ENGINE, DB_USERNAME, DB_PASSWORD, DB_HOST, DB_NAME)

engine = create_engine(
    SQLALCHEMY_DATABASE_URL
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def get_db():
    db = SessionLocal()
    try:
        return db
    finally:
        db.close()


def paginate(query, page: int) -> list:
    start_at = (page - 1) * 10
    return query.offset(start_at).limit(10).all()
