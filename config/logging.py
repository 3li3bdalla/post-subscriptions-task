import os

LOGGING_URI = "http://{}@logging-web:8000/1".format(os.getenv("ERROR_REPORTING_SENTRY_HASH", ""))
LOKI_URL = os.getenv("LOKI_URL", "http://loki:3100")
