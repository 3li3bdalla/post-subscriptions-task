import os
from datetime import datetime, timedelta

from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
import datetime
from database.models import User

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
from repositories.user import get_user_by_username

APP_JWT_SECRET = os.getenv("APP_JWT_SECRET")
APP_JWT_ALGORITH = os.getenv("APP_JWT_ALGORITH", "HS256")


async def get_current_user(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, APP_JWT_SECRET, algorithms=[APP_JWT_ALGORITH])
        username: str = payload.get("username")
        if username is None:
            raise credentials_exception
    except JWTError:
        raise credentials_exception
    user = get_user_by_username(username)
    if user is None:
        raise credentials_exception
    return user


def generate_jwt_token(created_user: User):
    created_user.token = jwt.encode(
        {
            "id": created_user.id,
            "username": created_user.username,
            "issued_at": "{}".format(datetime.datetime.now()),
            "exp": datetime.datetime.utcnow() + timedelta(hours=720)
        },
        APP_JWT_SECRET,
        algorithm=APP_JWT_ALGORITH)
    return created_user
