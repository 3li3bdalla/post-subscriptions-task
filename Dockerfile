FROM python:3.9
WORKDIR /app/code
RUN python -m pip install --upgrade pip
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 8080
RUN mkdir -p /app/code/logs
RUN touch /app/code/logs/app.log
RUN chmod 777 /app/code/logs/app.log
COPY . .
RUN chmod 777 ./build/deploy.sh
ENTRYPOINT ["/bin/sh","/app/code/build/deploy.sh"]