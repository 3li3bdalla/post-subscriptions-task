import datetime

from config.database import SessionLocal, engine, Base
import sentry_sdk
import logging
import uuid
from config.logging import LOGGING_URI, LOKI_URL
import logging_loki
from fastapi_redis_cache import FastApiRedisCache, cache
import os
from sqlalchemy.orm import Session
from fastapi import Request, Response


def bootstrap(app):
    setup_error_reporting_platform()
    monitor_requests(app)


# body_iterator
def setup_error_reporting_platform():
    sentry_sdk.init(LOGGING_URI, traces_sample_rate=1.0)


def monitor_requests(app):
    logger = get_logger()

    @app.middleware("http")
    async def log_stuff(request: Request, call_next):
        loggerId = uuid.uuid4()
        logger.debug("{} {} : {} {} {} {}".format(loggerId, datetime.datetime.now(), request.method, request.url,
                                                  request.path_params, request.query_params))
        response = await call_next(request)
        # logger.debug("{} {} : {}".format(loggerId, datetime.datetime.now(), response))
        logger.debug("{} {} : {}".format(loggerId, datetime.datetime.now(), response.status_code))
        return response


def setup_cache(app):
    @app.on_event("startup")
    def startup():
        redis_cache = FastApiRedisCache()
        redis_cache.init(
            host_url="redis://{}:6379".format(os.getenv("REDIS_HOST")),
            prefix="myapi-cache",
            response_header="X-MyAPI-Cache",
            ignore_arg_types=[Request, Response, Session]
        )


def get_logger():
    handler = logging_loki.LokiHandler(
        url="{}/loki/api/v1/push".format(LOKI_URL),
        tags={"application": "app"},
        version="1",
    )
    logger = logging.getLogger('app')
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger
